#!/bin/bash


#All this script will only apply to the ref module because the tutorial module does not seen to have any xrefs in it


#Replace the xrefs for internal references
sed -i 's/xref:.*\]/&>>/' emilua/doc/modules/ref/pages/*.adoc 
sed -i 's/xref:/<<_/' emilua/doc/modules/ref/pages/*.adoc


#appending the , inherent for the reference and exchange filename for section ID
sed -i 's/.adoc/&,/' emilua/doc/modules/ref/pages/*.adoc
sed -i 's/.adoc//g' emilua/doc/modules/ref/pages/*.adoc

#sed ATR GERX code using match_pattern
sed -i 's/\(<<_.*,\)\[/\1/g' emilua/doc/modules/ref/pages/*.adoc  # fist part, remove [

sed -i 's/\(<<_.*,.*\)\]/\1/g' emilua/doc/modules/ref/pages/*.adoc # remove last ]

sed -i 's/\(<<_[[:alpha:]]*\)\./\1_/g' emilua/doc/modules/ref/pages/*.adoc # replace dot from being match

#special commands for icons
sed -i s/$(echo -ne '\u2714')/icon:check[2x]/g emilua/doc/modules/ref/pages/*.adoc

sed -i s/$(echo -ne '\u2714')/icon:check[2x]/g emilua/doc/modules/tutorial/pages/*.adoc
